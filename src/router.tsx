import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom';
import { lazy, ReactNode, Suspense } from 'react';
import { Backdrop, CircularProgress } from '@mui/material';
import App from 'pages';
import LandingPage from 'pages/children/landingPage';

// const App = lazy(() => import('pages'));
// const LandingPage = lazy(() => import('pages/children/landingPage'));
const UniversityPage = lazy(() => import('pages/children/universityPage'));
const Contact = lazy(() => import('pages/children/contact'));
const President = lazy(() => import('pages/children/universityPage/children/president'));
const BachelorProgram = lazy(() => import('pages/children/bachelorProgram'));

function Wrapper({ children }: { children: ReactNode }) {
  return (
    <Suspense
      fallback={
        <Backdrop open sx={{ color: '#fff' }}>
          <CircularProgress color='inherit' />
        </Backdrop>
      }
    >
      {children}
    </Suspense>
  );
}

export default createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<App />}>
      <Route index element={<LandingPage />} />
      <Route
        path='university'
        element={
          <Wrapper>
            <UniversityPage />
          </Wrapper>
        }
      >
        <Route
          index
          element={
            <Wrapper>
              <President />
            </Wrapper>
          }
        />
      </Route>
      <Route
        path='contactUs'
        element={
          <Wrapper>
            <Contact />
          </Wrapper>
        }
      />
      <Route
        path='bachelorProgram'
        element={
          <Wrapper>
            <BachelorProgram />
          </Wrapper>
        }
      />
    </Route>,
  ),
);
