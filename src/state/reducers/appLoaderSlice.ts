import { createSlice } from '@reduxjs/toolkit';

const initialState: boolean = false;

const appLoaderSlice = createSlice({
  name: 'app loader',
  initialState,
  reducers: {
    startAppLoader: () => true,
    stopAppLoader: () => false,
  },
});

export const { stopAppLoader, startAppLoader } = appLoaderSlice.actions;
export const appLoaderReducer = appLoaderSlice.reducer;
