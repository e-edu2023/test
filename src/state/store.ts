import { configureStore } from '@reduxjs/toolkit';
import { appLoaderReducer } from './reducers';

const store = configureStore({
  reducer: {
    appLoader: appLoaderReducer,
  },
});
export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
