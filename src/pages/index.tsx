import './app.scss';
import { Outlet } from 'react-router-dom';
import Header from 'layout/header';
import Footer from 'layout/footer';

function App() {
  return (
    <div className='app_container'>
      <Header />
      <Outlet />
      <Footer />
    </div>
  );
}

export default App;
