import CustomBreadcrumbs from 'components/customBreadcrumbs';
import { Box, Stack, Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import socials from 'config/socials';
import CustomMap from 'components/map';
import styles from './contact.module.scss';

function Contact() {
  const { t } = useTranslation();

  return (
    <div className={styles.container}>
      <CustomBreadcrumbs />
      <Box p={{ xs: '1rem', md: '2rem 0' }}>
        <Typography fontSize={{ xs: '2rem' }} fontWeight={700} variant='h3' component='h2'>
          {t('contact.office')}
        </Typography>
      </Box>
      <Stack
        direction='row'
        gap={2}
        justifyContent={{ xs: 'start', sm: 'space-between' }}
        flexWrap='wrap'
        p={{ xs: '0 1rem', md: '0' }}
      >
        <div className={styles.contactBox}>
          <Typography variant='h6' fontWeight={600} gutterBottom>
            {t('contact.address')}
          </Typography>
          <Typography variant='subtitle2'>{t('footer.address')}</Typography>
        </div>
        <div className={styles.contactBox}>
          <Typography variant='h6' fontWeight={600} gutterBottom>
            {t('contact.contactUs')}
          </Typography>
          <Typography variant='subtitle2'>+998 95 131-55-55</Typography>
          <Typography variant='subtitle2'>info@tiuni.com</Typography>
        </div>
        <div className={styles.contactBox}>
          <Typography variant='h6' fontWeight={600} gutterBottom>
            {t('contact.businessHour')}
          </Typography>
          <Typography variant='subtitle2'>
            {t('contact.hourDays', { interval: '09:00 - 18:00' })}
          </Typography>
          <Typography color='red' variant='subtitle2'>
            {t('contact.weekend')}
          </Typography>
        </div>
        <div className={styles.contactBox}>
          <Typography variant='h6' fontWeight={600} gutterBottom>
            {t('contact.businessHour')}
          </Typography>
          <Stack direction='row' spacing={1} className={styles.socials}>
            {socials.map(social => (
              <a key={social.link} rel='noreferrer' href={social.link} target='_blank'>
                <img src={social.iconBlack} alt={social.link} />
              </a>
            ))}
          </Stack>
        </div>
      </Stack>
      <CustomMap />
    </div>
  );
}

export default Contact;
