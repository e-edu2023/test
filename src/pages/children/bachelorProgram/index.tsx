import CustomBreadcrumbs from 'components/customBreadcrumbs';
import { Stack, Typography } from '@mui/material';
import campus from 'assets/images/carousel/Image 7.jpg';
import { useTranslation } from 'react-i18next';
import directions from 'static/programs';
import frame from 'assets/icons/Icons/Frame.svg';
import getProgramData from 'static/programData';
import { useSearchParams } from 'react-router-dom';
import styles from './bachelorProgram.module.scss';

export default function BachelorProgram() {
  const { t } = useTranslation();
  const [searchParams] = useSearchParams();

  const key = searchParams.get('key');

  const data = getProgramData(key);

  return (
    <>
      <section className={styles.landing}>
        <div className={styles.content}>
          <CustomBreadcrumbs />
          <Stack direction='row' gap='8rem' p='8rem 0'>
            <div className={styles.info}>
              <Typography
                className={styles.directionName}
                fontWeight={700}
                variant='h3'
                component='h3'
                gutterBottom
              >
                {data.directionName}
              </Typography>
              <Typography variant='subtitle2' gutterBottom sx={{ mb: '5rem' }}>
                {data.directionInfo}
              </Typography>
              <a
                className={styles.link}
                href='https://qabul.tiu.uz/uz'
                target='_blank'
                rel='noreferrer'
              >
                {t('landingPage.carousel.content.apply')}
              </a>
            </div>
            <img className={styles.img} src={campus} alt='campus' />
          </Stack>
        </div>
      </section>
      <section className={styles.content}>
        <h1 className={styles.title}>{t('program.direction')}</h1>
        <Stack direction='row' gap='2rem' justifyContent='space-between' flexWrap='wrap'>
          {directions.map(direction => (
            <Stack direction='row' gap='0.6rem' key={direction.name} alignItems='start'>
              <img src={direction.icon} alt='direction' />
              <Stack>
                <Typography lineHeight='normal' fontWeight={600} variant='h6'>
                  {data[direction.name]}
                </Typography>
                <Typography variant='subtitle2'>{direction.description}</Typography>
              </Stack>
            </Stack>
          ))}
        </Stack>
        <h1 className={styles.title}>{t('program.about')}</h1>
        <Typography variant='subtitle2'>{data.directionDescription}</Typography>
        <h1 className={styles.title}>{t('program.careerProspects')}</h1>
        <Typography variant='subtitle2'>{data.careerProspect}</Typography>
        <Stack direction='row' gap='2rem' flexWrap='wrap' sx={{ p: '2rem 0' }}>
          {data.workPlaces.map(place => (
            <Stack direction='row' gap='0.6rem' key={place}>
              <img src={frame} alt='frame' />
              <Typography fontWeight={600} component='b'>
                {place}
              </Typography>
            </Stack>
          ))}
        </Stack>
        <h1 className={styles.title}>{t('program.applicationRequirements')}</h1>
        <Typography variant='subtitle2'>
          {`${data.directionName} bakalavriatiga hujjat topshirish uchun quyidagi hujjatlar va
          ma’lumotlar talab qilinadi`}
        </Typography>
        <Stack gap='1.5rem' justifyContent='space-between' sx={{ p: '2rem 0' }}>
          {data.requirements.map(requirement => (
            <Stack direction='row' gap='0.6rem' key={requirement.name} alignItems='start'>
              <img src={frame} alt='requirement' />
              <Stack>
                <Typography lineHeight='normal' fontWeight={600}>
                  {requirement.name}
                </Typography>
                <Typography variant='subtitle2'>{requirement.description}</Typography>
              </Stack>
            </Stack>
          ))}
        </Stack>
      </section>
    </>
  );
}
