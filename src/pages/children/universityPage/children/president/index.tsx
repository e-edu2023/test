import { Typography } from '@mui/material';
// import signature from 'assets/icons/Base/Signature.svg';
import president from 'assets/images/president.png';
// import point from 'assets/images/thesisPoint.png';

import './president.scss';

function President() {
  return (
    <div className='presidentContainer'>
      <div className='pContent'>
        <div className='leftSide'>
          <h1>President‘s message</h1>
          <p>
            Dear Students, Faculty, and Staff, It is with immense joy and pride that I extend a
            heartfelt welcome to each of you to the newly opened Tashkent International University!
            As the inaugural Rector of this prestigious institution, I am filled with optimism and
            excitement for the remarkable journey that lies ahead. The establishment of Tashkent
            International University marks a momentous milestone in the field of higher education,
            and I am honored to be part of this groundbreaking endeavor.
          </p>
          <p>
            To our pioneering students, you are the trailblazers who will shape the very fabric of
            this university's identity. Your decision to join us on this transformative educational
            path reflects your determination to embrace new challenges and chart a course for a
            future filled with limitless possibilities. I have no doubt that your passion for
            learning, your thirst for knowledge, and your dedication to personal growth will be the
            cornerstones of our collective success.
          </p>
          <p>
            To our esteemed faculty, I extend my deepest gratitude for joining us on this
            extraordinary journey. Your expertise, dedication, and unwavering commitment to academic
            excellence are the bedrock upon which Tashkent International University stands.
            Together, we will foster an environment that encourages intellectual curiosity, critical
            thinking, and creative expression, empowering our students to become leaders and change
            agents in a rapidly changing world.
          </p>
          <p>
            To the dedicated staff members, your behind-the-scenes efforts are invaluable as we
            shape this university into a vibrant community of learning. Your professionalism,
            efficiency, and kindness will ensure that our campus remains a welcoming and nurturing
            space for everyone. As we embark on this adventure together, I urge you all to embrace
            the spirit of collaboration, inclusivity, and innovation. Let us collectively build an
            institution that not only excels in academic pursuits but also values diversity, fosters
            empathy, and nurtures the holistic development of every individual.
          </p>
          <p>
            The journey ahead may present challenges, but with determination, resilience, and the
            support of one another, we will overcome them and emerge stronger. Let us celebrate our
            successes, learn from our experiences, and use every opportunity to grow and evolve as a
            community.
          </p>
          <p>
            Together, we will create a legacy that will inspire generations to come and leave an
            indelible mark on the landscape of higher education. Once again, I welcome you all to
            Tashkent International University with open arms and a profound sense of anticipation.
          </p>
          <p>Welcome to Tashkent International University!</p>
          {/* <div className='pThesis'> */}
          {/*  <div className='point'> */}
          {/*    <img src={point} alt='point' /> */}
          {/*  </div> */}
          {/*  <div className='text'> */}
          {/*    <h3> */}
          {/*      Oxford‘s vision is to build leading real estate businesses that create meaningful */}
          {/*      economic and social value for our customers and communities. */}
          {/*    </h3> */}
          {/*  </div> */}
          {/* </div> */}
          <div className='pConclusion'>
            <div className='pInfo'>
              <Typography variant='h6' fontWeight={600}>
                Dr. Jun Yong Wook
              </Typography>
              <Typography variant='body2' color='text.secondary'>
                Rector, Tashkent International University
              </Typography>
            </div>
            {/* <div className='pSignature'> */}
            {/*  <img src={signature} alt='signature' /> */}
            {/* </div> */}
          </div>
        </div>
        <div className='rightSide'>
          <img src={president} alt='president' />
        </div>
      </div>
    </div>
  );
}

export default President;
