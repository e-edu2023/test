import { Outlet } from 'react-router-dom';

function UniversityPage() {
  return (
    <div>
      <Outlet />
    </div>
  );
}

export default UniversityPage;
