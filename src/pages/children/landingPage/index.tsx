import Carousel from 'pages/children/landingPage/components/Carousel';
import Programs from 'pages/children/landingPage/components/Programs';
import News from 'pages/children/landingPage/components/news';
// import Events from 'pages/children/landingPage/components/events';
// import Universities from 'pages/children/landingPage/components/universities';
// import Services from 'pages/children/landingPage/components/services';
// import Facts from 'pages/children/landingPage/components/facts';
import Partners from 'pages/children/landingPage/components/partners';

function LandingPage() {
  return (
    <>
      <Carousel />
      <Programs />
      <News />
      <Partners />
    </>
  );
}

export default LandingPage;
