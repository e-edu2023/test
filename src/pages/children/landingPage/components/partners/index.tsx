import { useTranslation } from 'react-i18next';
import GenericTitle from 'components/genericTitle';
import { group, maktab, oliy, yoshlar } from 'assets';
import styles from './partners.module.scss';

const partners = [
  {
    img: oliy,
  },
  {
    img: yoshlar,
  },
  {
    img: maktab,
  },
  {
    img: group,
  },
];

function Partners() {
  const { t } = useTranslation();

  return (
    <div className={styles.content}>
      <GenericTitle>{t('landingPage.partners.title')}</GenericTitle>
      <div className={styles.contentCardsWrapper}>
        {partners.map((partner, i) => (
          <div className={styles.contentCard} key={i}>
            <img src={partner.img} alt='partner' />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Partners;
