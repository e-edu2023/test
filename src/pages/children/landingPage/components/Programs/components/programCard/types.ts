import { SvgIcon } from '@mui/material';

export interface IProgramCard {
  title: string;
  description: string;
  icon: typeof SvgIcon;
}
