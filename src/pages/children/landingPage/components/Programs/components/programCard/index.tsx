import { ReactNode } from 'react';
import { Stack, Typography } from '@mui/material';
import { ChevronRight } from '@mui/icons-material';

import { useNavigate } from 'react-router-dom';
import styles from './programcard.module.scss';

type Icon = string | ReactNode;

type ProgramCardProps = {
  title: string;
  direction: string;
  icon: Icon;
  type?: string;
  link: string;
  access: string;
};

function ProgramCard({ title, direction, icon, type, link, access }: ProgramCardProps) {
  const navigate = useNavigate();
  const goToProgram = () => {
    navigate(`/${link}?key=${access}`);
  };

  return (
    <Stack
      onClick={goToProgram}
      className={styles.card}
      direction='column'
      spacing={1}
      justifyContent='space-between'
    >
      <Stack direction='column' spacing='16px'>
        <Stack direction='row' justifyContent='space-between' alignItems='center'>
          {type ? <img src={icon as string} alt='language' /> : icon}
          <ChevronRight />
        </Stack>
        <Typography fontWeight={600} className={styles.cardContentTitle}>
          {title}
        </Typography>
      </Stack>
      <Typography variant='body2' color='text.secondary' noWrap>
        {direction}
      </Typography>
    </Stack>
  );
}

export default ProgramCard;
