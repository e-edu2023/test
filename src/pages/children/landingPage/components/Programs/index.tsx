import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import GenericTitle from 'components/genericTitle';
import CustomButton from 'components/customButton';
import programs from 'config/programs';
import classNames from 'classnames';
import ProgramCard from './components/programCard';
import './programs.scss';

function Programs() {
  const { t } = useTranslation();
  const [more, setMore] = useState(true);

  const toggle = () => setMore(!more);

  return (
    <div className='programsContainer' id='bachelorProgram'>
      <div className='programsContent'>
        <GenericTitle>{t('landingPage.programs.title')}</GenericTitle>
        <div
          className={classNames('programsCardsWrapper', {
            opened: more,
            closed: !more,
          })}
        >
          {programs.map(card => (
            <ProgramCard
              key={card.title}
              access={card.title}
              link={card.link}
              title={t(`programs.${card.title}`)}
              direction={t(`programs.${card.direction}`)}
              type={card.type}
              icon={card.icon}
            />
          ))}
        </div>
        <div className='moreBtn'>
          <CustomButton
            onClick={toggle}
            disableElevation
            color='info'
            variant='contained'
            shape='round'
            fullWidth
          >
            {more ? t('programs.viewMore') : t('programs.viewLess')}
          </CustomButton>
        </div>
      </div>
    </div>
  );
}

export default Programs;

//
// <div className='programsCardsCarouselWrapper'>
//   <FlexibleCarousel>
//     {programs.map((card, i) => (
//       <SwiperSlide>
//         <ProgramCard
//           key={i}
//           type={card.type}
//           title={t(`programs.${card.title}`)}
//           direction={t(`programs.${card.direction}`)}
//           icon={card.icon}
//         />
//       </SwiperSlide>
//     ))}
//   </FlexibleCarousel>
// </div>
