import { useTranslation } from 'react-i18next';
import styles from './carouselContent.module.scss';

export default function CarouselContent() {
  const { t } = useTranslation();

  return (
    <div className={styles.container}>
      <h1>{t('landingPage.carousel.content.title')}</h1>
      <p>{t('landingPage.carousel.content.description')}</p>
      <div>
        <a href='https://qabul.tiu.uz/uz' rel='noreferrer' target='_blank'>
          {t('landingPage.carousel.content.apply')}
        </a>
      </div>
    </div>
  );
}
