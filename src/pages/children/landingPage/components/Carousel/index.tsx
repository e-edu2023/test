// import { useRef } from 'react';
import 'swiper/scss';
// import { Swiper, SwiperSlide, SwiperClass } from 'swiper/react';
import carouselList from 'config/carouselList';
// import { Button } from '@mui/material';
// import { arrowLeftIcon, arrowRightIcon } from 'assets';
// import cn from 'classnames';
// import FlexibleCarousel from 'components/FlexibleCarousel';
// import { useTranslation } from 'react-i18next';
import CarouselContent from './components/carouselContent';
import styles from './carousel.module.scss';

function Carousel() {
  // const { t } = useTranslation();
  // const swiperRef = useRef<SwiperClass>();
  // const [activeSlide, setActiveSlide] = useState<number>(0);

  // const onContentClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   const target = event.target as HTMLButtonElement;
  //   if (target.dataset.id) {
  //     setActiveSlide(Number(target.dataset.id));
  //     swiperRef.current?.slideTo(Number(target.dataset.id));
  //   }
  // };

  // const slidePrev = () => {
  //   if (activeSlide === 0) {
  //     const currentSlide = carouselList.length - 1;
  //     swiperRef.current?.slideTo(currentSlide);
  //     setActiveSlide(currentSlide);
  //   } else {
  //     swiperRef.current?.slideTo(activeSlide - 1);
  //     setActiveSlide(activeSlide - 1);
  //   }
  // };
  // const slideNext = () => {
  //   if (activeSlide === carouselList.length - 1) {
  //     swiperRef.current?.slideTo(0);
  //     setActiveSlide(0);
  //   } else {
  //     swiperRef.current?.slideTo(activeSlide + 1);
  //     setActiveSlide(activeSlide + 1);
  //   }
  // };

  return (
    <div className={styles.container}>
      {/* <Swiper */}
      {/*  loop */}
      {/*  lazyPreloadPrevNext={0} */}
      {/*  slidesPerView={1} */}
      {/*  onSwiper={swiper => { */}
      {/*    swiperRef.current = swiper; */}
      {/*  }} */}
      {/* > */}
      {/*  {carouselList.map(item => ( */}
      {/*    <SwiperSlide key={item.id}></SwiperSlide> */}
      {/*  ))} */}
      {/* </Swiper> */}
      <img className={styles.image} src={carouselList[0].image} alt={carouselList[0].title} />

      <div className={styles.contentContainer}>
        <CarouselContent />

        {/* <div className={styles.swiperBottomContentContainer}> */}
        {/*  <div className={styles.swiperBottomContent}> */}
        {/*    <FlexibleCarousel> */}
        {/*      {carouselList.map(item => ( */}
        {/*        <SwiperSlide key={item.id}> */}
        {/*          <div */}
        {/*            className={cn(styles.textBox, { */}
        {/*              [styles.activeIndex]: activeSlide === item.id, */}
        {/*            })} */}
        {/*          > */}
        {/*            <Button */}
        {/*              disableRipple */}
        {/*              color='inherit' */}
        {/*              onClick={onContentClick} */}
        {/*              data-id={item.id} */}
        {/*            > */}
        {/*              {t(`landingPage.carousel.pagination.${item.title}`)} */}
        {/*            </Button> */}
        {/*          </div> */}
        {/*        </SwiperSlide> */}
        {/*      ))} */}
        {/*    </FlexibleCarousel> */}
        {/*  </div> */}
        {/*  <div className={styles.swiperNavigation}> */}
        {/*    <Stack direction='row' spacing={1}> */}
        {/*      <IconButton onClick={slidePrev} size='large'> */}
        {/*        <img src={arrowLeftIcon} alt='arrowLeftIcon' /> */}
        {/*      </IconButton> */}
        {/*      <IconButton onClick={slideNext} size='large'> */}
        {/*        <img src={arrowRightIcon} alt='arrowRightIcon' /> */}
        {/*      </IconButton> */}
        {/*    </Stack> */}
        {/*  </div> */}
        {/* </div> */}
      </div>
    </div>
  );
}

export default Carousel;
