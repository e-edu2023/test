import React from 'react';
import Instagram from './instagram';
import Youtube from './youtube';

const Social: React.FC = () => {
  return (
    <>
      <Instagram />
      <Youtube />
    </>
  );
};

export default Social;
