import React from 'react';
import { useTranslation } from 'react-i18next';
import { SwiperSlide } from 'swiper/react';
import GenericContainer, { IGenericTitleProps } from '../components/genericContainer';
import styles from './youtube.module.scss';

const Youtube: React.FC = () => {
  const { t } = useTranslation();

  const props: IGenericTitleProps = {
    headTitle: t('landingPage.social.youtube.title'),
    contentTitle: 'Tashkent International University ',
    contentDescription: '@tiu_university',
    contentCount: '274 K subscribers',
    buttonText: t('landingPage.social.youtube.subscribe'),
  };

  return (
    <GenericContainer {...props}>
      {list.map((item, index) => (
        <SwiperSlide key={index}>
          <div className={styles.videoBox} key={index}>
            <iframe allowFullScreen src={item.link} title={item.link} />
          </div>
        </SwiperSlide>
      ))}
    </GenericContainer>
  );
};

export default Youtube;

const list = [
  {
    link: 'https://www.youtube.com/embed/kZ3psfSSu5k',
  },
  {
    link: 'https://www.youtube.com/embed/kZ3psfSSu5k',
  },
  {
    link: 'https://www.youtube.com/embed/kZ3psfSSu5k',
  },
];
