import React from 'react';
import { useTranslation } from 'react-i18next';
import { SwiperSlide } from 'swiper/react';
import { instagram } from 'assets';
import GenericContainer, { IGenericTitleProps } from '../components/genericContainer';
import styles from './instagram.module.scss';

const Instagram: React.FC = () => {
  const { t } = useTranslation();

  const props: IGenericTitleProps = {
    headTitle: t('landingPage.social.instagram.title'),
    contentTitle: 'tiu_university',
    contentDescription: 'TIU',
    contentCount: '1,2 M followers',
    buttonText: t('landingPage.social.instagram.following'),
  };

  return (
    <GenericContainer {...props}>
      {list.map((item, index) => (
        <SwiperSlide key={index}>
          <div className={styles.imageBox}>
            <img src={item.img} alt='instagram' />
          </div>
        </SwiperSlide>
      ))}
    </GenericContainer>
  );
};

export default Instagram;

const list = [
  {
    img: instagram,
  },
  {
    img: instagram,
  },
  {
    img: instagram,
  },
  {
    img: instagram,
  },
];
