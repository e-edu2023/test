import { PropsWithChildren } from 'react';
import { Button, Typography } from '@mui/material';
import GenericTitle from 'components/genericTitle';
import FlexibleCarousel from 'components/FlexibleCarousel';
import { avatar } from 'assets';
import styles from './genericContainer.module.scss';

export interface IGenericTitleProps extends PropsWithChildren {
  headTitle: string;
  contentTitle: string;
  contentDescription: string;
  buttonText: string;
  contentCount: string;
}

function GenericContainer({
  children,
  headTitle,
  contentTitle,
  contentDescription,
  contentCount,
  buttonText,
}: IGenericTitleProps) {
  return (
    <div className={styles.content}>
      <GenericTitle>{headTitle}</GenericTitle>

      <div className={styles.info}>
        <img src={avatar} alt='avatar' />
        <div>
          <Typography variant='h5' noWrap>
            {contentTitle}
          </Typography>
          <Typography variant='body2' color='text.secondary' noWrap>
            {contentDescription} <span className={styles.dot}>&#183;</span> {contentCount}
          </Typography>
        </div>
        <Button color='inherit'>{buttonText}</Button>
      </div>
      <FlexibleCarousel spaceBetween={12}>{children}</FlexibleCarousel>
    </div>
  );
}

export default GenericContainer;
