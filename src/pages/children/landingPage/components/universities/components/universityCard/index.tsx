import { Typography, CardContent, Card, CardMedia, Box } from '@mui/material';
import { IUniversity } from './types';

interface IUniversityCardProps {
  university: IUniversity;
}

function UniversityCard({ university }: IUniversityCardProps) {
  return (
    <Card elevation={0}>
      <CardMedia
        component='img'
        height='160'
        sx={{ minWidth: '280px' }}
        image={university.image}
        alt={university.title}
      />
      <CardContent>
        <Box sx={{ marginBottom: '10px' }}>
          <img src={university.logo} alt={university.title} loading='lazy' />
        </Box>
        <Typography variant='h6' component='div' sx={{ fontWeight: 600 }}>
          {university.title}
        </Typography>
        <Typography variant='body2' color='text.secondary'>
          {university.description}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default UniversityCard;
export * from './types';
