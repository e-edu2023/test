export interface IUniversity {
  title: string;
  description: string;
  image: string;
  logo: string;
}
