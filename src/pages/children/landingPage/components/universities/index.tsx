import { useTranslation } from 'react-i18next';
import './universities.scss';
import { SwiperSlide } from 'swiper/react';
import { post1, universityLogo } from 'assets';
import GenericTitle from 'components/genericTitle';
import FlexibleCarousel from 'components/FlexibleCarousel';
import UniversityCard, { IUniversity } from './components/universityCard';

function Universities() {
  const { t } = useTranslation();

  return (
    <div className='universitiesContainer'>
      <div className='universitiesContent'>
        <GenericTitle>{t('landingPage.universities.title')}</GenericTitle>
        <div className='universitiesCardsWrapper'>
          {universities.map((university, key) => (
            <UniversityCard key={key} university={university} />
          ))}
        </div>
        <div className='universitiesCardsCarouselWrapper'>
          <FlexibleCarousel>
            {universities.map((university, i) => (
              <SwiperSlide key={i}>
                <UniversityCard university={university} />
              </SwiperSlide>
            ))}
          </FlexibleCarousel>
        </div>
      </div>
    </div>
  );
}

export default Universities;

const universities: IUniversity[] = [
  {
    title: 'Solbridge University',
    description: 'South Korea',
    image: post1,
    logo: universityLogo,
  },
  {
    title: 'Solbridge University',
    description: 'South Korea',
    image: post1,
    logo: universityLogo,
  },
  {
    title: 'Solbridge University',
    description: 'South Korea',
    image: post1,
    logo: universityLogo,
  },
];
