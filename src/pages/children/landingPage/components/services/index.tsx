import React from 'react';
import { useTranslation } from 'react-i18next';
import { ChevronRight, AttachMoney } from '@mui/icons-material';
import GenericTitle from 'components/genericTitle';
import ServiceCard, { IService } from './components/serviceCard';
import styles from './services.module.scss';

const Services: React.FC = () => {
  const { t } = useTranslation();

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <GenericTitle>{t('landingPage.services.title')}</GenericTitle>

        <div className={styles.serviceCardWrapper}>
          {services.map((service, key) => (
            <ServiceCard key={key} service={service} />
          ))}
        </div>
        <div className={styles.serviceLink}>
          <div>
            {t('landingPage.services.allServices')} <ChevronRight />
          </div>
        </div>
        <div className={styles.serviceCardMoreBox}>
          <button>More</button>
        </div>
      </div>
    </div>
  );
};

export default Services;

const services: IService[] = [
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
  {
    title: 'Payment contract',
    description:
      'On the form below, please choose the method of payment that you plan to use to pay your tuition.',
    icon: AttachMoney,
  },
];
