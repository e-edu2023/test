import { SvgIcon } from '@mui/material';

export interface IService {
  title: string;
  description: string;
  icon: typeof SvgIcon;
}
