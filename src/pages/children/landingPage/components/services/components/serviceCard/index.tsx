import { Card, Typography } from '@mui/material';
import { ChevronRight } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';
import { IService } from './types';
import './serviceCard.scss';

interface IServiceCardProps {
  service: IService;
}

function ServiceCard({ service }: IServiceCardProps) {
  const { t } = useTranslation();

  return (
    <Card elevation={0} sx={{ borderRadius: '1rem' }} className='serviceCardContainer'>
      <div className='serviceCardContent'>
        <div className='serviceCardIcon'>
          <div>
            <service.icon />
          </div>
        </div>
        <div>
          <h3 className='serviceCardTitle'>{service.title}</h3>
          <Typography variant='body2' color='text.secondary' gutterBottom>
            {service.description}
          </Typography>
          <div className='serviceCardLink'>
            {t('landingPage.services.learnMore')} <ChevronRight />
          </div>
        </div>
      </div>
    </Card>
  );
}

export default ServiceCard;
export * from './types';
