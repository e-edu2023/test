import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';

import GenericTitle from 'components/genericTitle';
import './facts.scss';

const statistics = [
  {
    count: '280+',
    description: 'Bank managers teach in the Leaders Teach Leaders system',
  },
  {
    count: '250+',
    description: 'Bank managers teach',
  },
  {
    count: '28+',
    description: 'Bank managersLeaders system',
  },
  {
    count: '80+',
    description: 'Bank the Leaders Teach Leaders system',
  },
];

function Facts() {
  const { t } = useTranslation();

  return (
    <div className='factsContainer'>
      <div className='factsContent'>
        <GenericTitle>{t('landingPage.facts.title')}</GenericTitle>

        <div className='factsInfoBox'>
          <div className='factsInfoText'>
            <p>{t('landingPage.facts.description')}</p>
          </div>
          <div className='factsStatistics'>
            {statistics.map(item => (
              <div key={item.count}>
                <span>{item.count}</span>
                <Typography variant='body2' color='text.secondary'>
                  {item.description}
                </Typography>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Facts;
