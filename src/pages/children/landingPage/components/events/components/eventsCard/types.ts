export interface IEventsCard {
  title: string;
  time: string;
  place: string;
  date: string;
}
