import { Card, Grid, Typography } from '@mui/material';
import { IEventsCard } from './types';
import './eventsCard.scss';

interface IEventsCardProps {
  eventsCard: IEventsCard;
}

function EventsCard({ eventsCard }: IEventsCardProps) {
  return (
    <Card elevation={0}>
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <div className='eventsCardDateBox'>
            <h5>MAY</h5>
            <h1>29</h1>
          </div>
        </Grid>
        <Grid item xs={10}>
          <div className='eventsCardTextBox'>
            <h3 className='eventsCardTitle'>{eventsCard.title}</h3>
            <Typography variant='body2' className='eventsCardDescription'>
              {eventsCard.time}
              <span className='eventsCardDescriptionDot'>&#183;</span>
              {eventsCard.place}
            </Typography>
          </div>
        </Grid>
      </Grid>
    </Card>
  );
}

export default EventsCard;
