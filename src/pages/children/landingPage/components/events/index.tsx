import { useTranslation } from 'react-i18next';
import { ChevronRight } from '@mui/icons-material';

import GenericTitle from 'components/genericTitle';
import styles from './events.module.scss';
import EventsCard from './components/eventsCard';
import { IEventsCard } from './components/eventsCard/types';

const news: IEventsCard[] = [
  {
    title:
      'The ‘Transformation of Career Cesdfnters 2.0‘ Project-Analytical Session was held at TI University',
    time: '14:00 - 16:00',
    place: 'Feinstone Hall',
    date: '2023-06-07T05:48:08.760Z',
  },
  {
    title:
      'The ‘Transformation of Career Centdsfers 2.0‘ Project-Analysdftical Session was held at TI University',
    time: '14:00 - 16:00',
    place: 'Feinstone Hall',
    date: '2023-06-07T05:48:08.760Z',
  },
  {
    title:
      'The ‘Transformation of Career Centers 2.0‘ Projsdfect-Analytical Session was held at TI University',
    time: '14:00 - 16:00',
    place: 'Feinstone Hall',
    date: '2023-06-07T05:48:08.760Z',
  },
];

function Events() {
  const { t } = useTranslation();

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <GenericTitle>{t('landingPage.events.title')}</GenericTitle>

        <div className={styles.eventsCardWrapper}>
          {news.map(eventsCard => (
            <EventsCard key={eventsCard.title} eventsCard={eventsCard} />
          ))}
        </div>
        <div className={styles.eventsLink}>
          <div>
            {t('landingPage.events.allEvents')} <ChevronRight />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Events;
