import { useTranslation } from 'react-i18next';
import GenericTitle from 'components/genericTitle';
import law from 'assets/news/law.jpg';
import virtual from 'assets/news/photo_2023-07-26_14-01-58.jpg';
import students from 'assets/news/students.png';
import NewsCard, { INewsCard } from './components/newsCard';
import styles from './news.module.scss';

const news: INewsCard[] = [
  {
    title: "Tashkent international universitetida 2023/2024 o'quv yili uchun qabul davom etmoqda!",
    date: '23 May 2023',
    image: students,
    link: 'http://qabul.tiu.uz',
  },
  {
    title: 'Tashkent international universityga virtual sayohat',
    date: '23 May 2023',
    image: virtual,
    link: 'https://www.youtube.com/watch?v=WiBdgHBmVrs',
  },
  {
    title:
      "Toshkent xalqaro universitetini tashkil etish bo'yicha Vazirlar Mahkamasining 900-sonli qarori",
    date: '23 May 2023',
    image: law,
    link: 'https://lex.uz/uz/docs/-4584249',
  },
];

function News() {
  const { t } = useTranslation();

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <GenericTitle>{t('landingPage.news.title')}</GenericTitle>
        <div className={styles.newsCardWrapper}>
          {news.map(newsCard => (
            <NewsCard key={newsCard.title} newsCard={newsCard} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default News;
// <div className={styles.newsLink}>
//   <div>
//     {t('landingPage.news.allNews')} <ChevronRight />
//   </div>
// </div>
