export interface INewsCard {
  image: string;
  title: string;
  date: string;
  link: string;
}
