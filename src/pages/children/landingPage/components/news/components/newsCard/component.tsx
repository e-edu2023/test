import { Card, Typography } from '@mui/material';
import { INewsCard } from './types';
import s from './newsCard.module.scss';

type NewsCardProps = {
  newsCard: INewsCard;
};

const NewsCard = ({ newsCard }: NewsCardProps) => (
  <Card elevation={0}>
    <a href={newsCard.link} target='_blank' className={s.newsCardWrapper} rel='noreferrer'>
      <div className={s.newsCardImageBox}>
        <img className={s.newsCardImage} src={newsCard.image} alt='news card' />
      </div>
      <div className={s.newsCardText}>
        <Typography className={s.newsCardTitle} gutterBottom fontWeight={600}>
          {newsCard.title}
        </Typography>
        <Typography variant='body2' color='text.secondary'>
          {newsCard.date}
        </Typography>
      </div>
    </a>
  </Card>
);

export default NewsCard;
