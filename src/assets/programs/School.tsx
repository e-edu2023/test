export default function School() {
  return (
    <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g id='Icons / school-line'>
        <path
          id='Vector'
          d='M16.0007 1.33325H24.0007V12.0002H29.334V25.3335H30.6673V28.0002H1.33398V25.3335H2.66732V12.0002H8.00065V1.33325H16.0007ZM24.0007 25.3335H26.6673V14.6669H24.0007V25.3335ZM8.00065 14.6669H5.33398V25.3335H8.00065V14.6669ZM10.6673 3.99992V25.3335H14.6673V8.00019H17.334V25.3335H21.334V3.99992H16.0007H10.6673Z'
          fill='#5070EE'
        />
      </g>
    </svg>
  );
}
