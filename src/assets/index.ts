import logo from './images/header/logo.png';
import langIcon from './images/header/earth.png';
import searchIcon from './images/header/search.png';
import arrowDownIcon from './images/header/arrowDown.png';
import arrowRightIcon from './images/carousel/arrowRight.png';
import arrowLeftIcon from './images/carousel/Vector (Stroke).png';

import post1 from './images/carousel/Image 7.jpg';
import post2 from './images/carousel/Image 6.jpg';
import post3 from './images/carousel/Image 5.jpg';
import post4 from './images/carousel/Image 4.jpg';
import post5 from './images/carousel/Image 3.jpg';

import maktab from './partners/maktab.png';
import oliy from './partners/oliyTalim.png';
import yoshlar from './partners/bmba_logo_3-removebg-preview.png';
import group from './partners/logoSecondaryDark.svg';

import avatar from './images/Avatar.png';
import instagram from './images/Item.png';
import universityLogo from './Card/Logo.png';

export {
  logo,
  langIcon,
  searchIcon,
  arrowDownIcon,
  arrowRightIcon,
  arrowLeftIcon,
  post1,
  post2,
  post3,
  post4,
  post5,
  maktab,
  oliy,
  yoshlar,
  group,
  avatar,
  instagram,
  universityLogo,
};
