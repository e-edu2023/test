import { Breadcrumbs } from '@mui/material';
import { NavigateNext } from '@mui/icons-material';
import { useLocation, Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import styles from './customBreadCrumbs.module.scss';

function CustomBreadcrumbs() {
  const { pathname, search } = useLocation();
  const { t } = useTranslation();

  const routes = `${pathname}${search}`.split('/');
  const links = pathname.split('/');

  return (
    <Breadcrumbs separator={<NavigateNext fontSize='small' />} aria-label='breadcrumb'>
      {links.map((route, index) => (
        <Link className={styles.link} key={route} to={`/${routes[index]}`}>
          {t(`header.menu.${route || 'home'}`)}
        </Link>
      ))}
    </Breadcrumbs>
  );
}

export default CustomBreadcrumbs;
