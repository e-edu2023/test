import { Button, ButtonProps } from '@mui/material';

type ButtonShape = 'round' | 'square';

export interface CustomButtonProps extends ButtonProps {
  shape?: ButtonShape;
}

export default function CustomButton({ shape, children, ...rest }: CustomButtonProps) {
  if (shape) {
    return (
      <Button sx={{ borderRadius: '1.5rem' }} {...rest}>
        {children}
      </Button>
    );
  }

  return <Button {...rest}>{children}</Button>;
}
