import { YMaps, Map as YandexMap, Placemark } from '@pbe/react-yandex-maps';
import { defaultMapCenter, defaultMapState, YANDEX_QUERY } from 'config';
import { Stack, Typography } from '@mui/material';
import dot from 'assets/icons/Icons/Ellipse 11.svg';
import bus from 'assets/icons/Icons/bus.svg';
import miniBus from 'assets/icons/Icons/minibuses.svg';
import metro from 'assets/icons/Icons/metro.svg';
import s from './map.module.scss';

const data = [
  {
    title: 'Avtobus',
    text: '88 · 85 · 152',
    icon: bus,
  },
  {
    title: 'Marshrutka',
    text: '100m',
    icon: miniBus,
  },
  {
    title: 'Metro',
    text: (
      <b>
        <img src={dot} alt='dot' /> Minor {' · '} <img src={dot} alt='dot' /> Bodomzor
      </b>
    ),
    icon: metro,
  },
];

export default function CustomMap() {
  return (
    <div className={s.container}>
      <YMaps query={YANDEX_QUERY}>
        <YandexMap
          className={s.map}
          modules={['control.ZoomControl', 'control.FullscreenControl']}
          defaultState={defaultMapState}
        >
          <Placemark geometry={defaultMapCenter} />
        </YandexMap>
      </YMaps>
      <div className={s.floatBox}>
        <Stack direction='row' gap='1rem' flexWrap='wrap'>
          {data.map(item => (
            <Stack key={item.title}>
              <Typography fontSize={14} variant='subtitle2'>
                {item.title}
              </Typography>
              <Typography
                lineHeight='normal'
                fontWeight={700}
                display='flex'
                alignItems='center'
                gap='0.4rem'
              >
                <img src={item.icon} alt='bus' />
                <span>{item.text}</span>
              </Typography>
            </Stack>
          ))}
        </Stack>
      </div>
    </div>
  );
}
