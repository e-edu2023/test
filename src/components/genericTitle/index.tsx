import styles from './styles.module.scss';

type GenericTitleProps = {
  children: string;
};

function GenericTitle({ children }: GenericTitleProps) {
  return <h1 className={styles.title}>{children}</h1>;
}

export default GenericTitle;
