import { PropsWithChildren } from 'react';
import { Swiper } from 'swiper/react';
import 'swiper/scss';

import './fCarousel.scss';

type FlexibleCarouselProps = {
  spaceBetween?: number;
};

function FlexibleCarousel({
  children,
  spaceBetween = 18,
}: PropsWithChildren<FlexibleCarouselProps>) {
  return (
    <Swiper slidesPerView='auto' spaceBetween={spaceBetween}>
      {children}
    </Swiper>
  );
}

export default FlexibleCarousel;
