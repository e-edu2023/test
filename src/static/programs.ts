import book from 'assets/icons/Icons/book-open.svg';
import calendar from 'assets/icons/Icons/calendar.svg';
import key from 'assets/icons/Icons/Keys.svg';
import bag from 'assets/icons/Icons/briefcase-1.svg';
import dollar from 'assets/icons/Icons/dollar-sign 1.svg';

type Direction = {
  name: 'type' | 'duration' | 'credits' | 'semesters' | 'annualFee';
  description: string;
  icon: string;
};

const directions: Direction[] = [
  {
    name: 'type',
    description: "Ta'lim turi",
    icon: book,
  },
  {
    name: 'duration',
    description: 'Davomiyligi',
    icon: calendar,
  },
  {
    name: 'credits',
    description: 'Kredit miqdori',
    icon: key,
  },
  {
    name: 'semesters',
    description: 'Semestr',
    icon: bag,
  },
  {
    name: 'annualFee',
    description: 'Yillik kontrakt miqdori',
    icon: dollar,
  },
];

export default directions;
