const foreignLanguageEnglish = {
  directionName: 'Xorijiy til va adabiyoti (ingliz tili)',
  directionInfo:
    "Xorijiy til va adabiyoti (ingliz tili) bakalavriat ta'lim yo'nalishi - kasb-hunar kollejlari, akademik litseylar, va umumta’lim maktablari o`quvchilariga chet tillarini o'rgatish ko`nikma va malakalarini shakllantirishga ko'maklashuvchi o'quv dasturi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '20,000,000 UZS',
  directionDescription:
    "Xorijiy til va adabiyoti (ingliz tili) bakalavriat ta'lim yo'nalishi - kasb-hunar kollejlari, akademik litseylar, va umumta’lim maktablari o`quvchilariga chet tillarini o'rgatish ko`nikma va malakalarini shakllantirishga ko'maklashuvchi o'quv dasturi.",
  careerProspect:
    "Xorijiy til va adabiyoti ta'lim yo'nalishi bitiruvchilari ta'lim tashkilotlarida malakali til o'qituvchisi, turizm sohasida malakali tarjimon hamda soha bo'yicha tegishli tashkilotlarda xorijiy til va adabiyoti bo'yicha mutahassis kabi faoliyat turlari bilan shugʼullanadi",
  workPlaces: [
    "Ta'lim tashkilotlari",
    'turizm tashkilotlari',
    'tarmoq faoliyatiga doir tashkilotlarda',
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const foreignLanguageKorean = {
  directionName: 'Xorijiy til va adabiyoti (koreys tili)',
  directionInfo:
    "Xorijiy til va adabiyoti (koreys tili) bakalavriat ta'lim yo'nalishi - koreys tilshunosligi, klassik koreys adabiyoti va zamonaviy koreys adabiyoti va ularning asosiy xususiyatlarini tushuntirib beruvchi o'quv dasturi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '20,000,000 UZS',
  directionDescription:
    "Xorijiy til va adabiyoti (koreys tili) bakalavriat ta'lim yo'nalishi - koreys tilshunosligi, klassik koreys adabiyoti va zamonaviy koreys adabiyoti va ularning asosiy xususiyatlarini tushuntirib beruvchi o'quv dasturi.",
  careerProspect:
    "Xorijiy til va adabiyoti ta'lim yo'nalishi bitiruvchilari ta'lim tashkilotlarida malakali til o'qituvchisi, turizm sohasida malakali tarjimon hamda soha bo'yicha tegishli tashkilotlarda xorijiy til va adabiyoti bo'yicha mutahassis kabi faoliyat turlari bilan shugʼullanadi",
  workPlaces: [
    "Ta'lim tashkilotlari",
    'turizm tashkilotlari',
    'tarmoq faoliyatiga doir tashkilotlarda',
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const preSchool = {
  directionName: 'Maktabgacha taʼlim',
  directionInfo:
    "Maktabgacha ta'lim bakalavr ta'lim yo'nalishi - maktabgacha ta’lim tashkilotlari, o‘rta maxsus, kasb-hunar ta’limi muassasalari, xususiy ta’lim tashkilotlari, pedagogik faoliyat vositalari, usullari, metodlari qo‘llaniladigan sohalarni o'z ichiga oluvchi o'quv dasturi",
  type: 'Kunduzgi',
  duration: '3 yil',
  credits: 180,
  semesters: 8,
  annualFee: '18,000,000 UZS',
  directionDescription:
    'Maktabgacha ta’lim tashkilotlarida faoliyat yuritish, maktabdan tashqari ta’lim tashkilotlari, o’rta maxsus, profisional ta’lim tizimida mutaxassislik fanlaridan dars berish, ijtimoiy-gumanitar yo’nalishdagi ilmiy-tadqiqot tashkilotlarida kichik ilmiy xodim bo‘lib ishlash, maktabgacha ta’lim tizimining vakolatli organlarida, kasbiy ta’lim pedagogikasi va texnologiyasi, o‘qitishning didaktik vositalarini ishlab chiqarish va amaliyotga joriy etish sohalarida kasbiy faoliyat turlari bilan shug‘ullanadi.',
  careerProspect:
    'Maktabgacha ta’lim tashkilotlarida faoliyat yuritish, maktabdan tashqari ta’lim tashkilotlari, o’rta maxsus, profisional ta’lim tizimida mutaxassislik fanlaridan dars berish, ijtimoiy-gumanitar yo’nalishdagi ilmiy-tadqiqot tashkilotlarida kichik ilmiy xodim bo‘lib ishlash, maktabgacha ta’lim tizimining vakolatli organlarida, kasbiy ta’lim pedagogikasi va texnologiyasi, o‘qitishning didaktik vositalarini ishlab chiqarish va amaliyotga joriy etish sohalarida kasbiy faoliyat turlari bilan shug‘ullanadi.',
  workPlaces: [
    'Maktabgacha ta’lim tashkilotlari',
    'o‘rta maxsus, kasb-hunar ta’limi muassasalari',
    ' xususiy ta’lim tashkilotlari',
    'pedagogik faoliyat vositalari',
    'usullari',
    'metodlari qo‘llaniladigan sohalar',
    'maktabgacha ta’lim tizimining vakolatli organlarida',
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const psychology = {
  directionName: 'Psixologiya',
  directionInfo:
    "Psixologiya bakalavr ta'lim yo'nalishi -  talabalarni ishchi kuchidagi boshlang'ich darajadagi ishlarga va psixologiya bo'yicha ilg'or kasbiy ta'limga tayyorlovchi, psixologiya ixtisosligi talabalarni psixologiyaning har bir asosiy sohalari bilan tanishtiruvchi o'quv dasturi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '18,000,000 UZS',
  directionDescription:
    "Psixologiya mutaxassisligi talabalarni ishchi kuchidagi boshlang'ich darajadagi ishlarga va psixologiya bo'yicha ilg'or kasbiy ta'limga tayyorlaydi. Psixologiya ixtisosligi talabalarni psixologiyaning har bir asosiy sohalari bilan tanishtiradi va talabalarga ushbu sohalarning har biri bo'yicha mustahkam bilim bazasini beradi. Bu talabalarni bilimlarni birlashtirish va qo'llashga undaydi va talabalarga o'z martaba maqsadlariga erishishda yordam berish uchun kurs tanlashda moslashuvchanlikni ta'minlaydi.",
  careerProspect:
    "Psixologiya mutaxassisligi talabalarni ishchi kuchidagi boshlang'ich darajadagi ishlarga va psixologiya bo'yicha ilg'or kasbiy ta'limga tayyorlaydi. Psixologiya ixtisosligi talabalarni psixologiyaning har bir asosiy sohalari bilan tanishtiradi va talabalarga ushbu sohalarning har biri bo'yicha mustahkam bilim bazasini beradi. Bu talabalarni bilimlarni birlashtirish va qo'llashga undaydi va talabalarga o'z martaba maqsadlariga erishishda yordam berish uchun kurs tanlashda moslashuvchanlikni ta'minlaydi.",
  workPlaces: [
    'Maktabgacha ta’lim tashkilotlari',
    'o‘rta maxsus',
    'kasb-hunar ta’limi muassasalarida psixolog',
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const primaryEducation = {
  directionName: 'Boshlangʻich taʼlim ',
  directionInfo:
    "Boshlang'ich ta'lim bakalavr ta'lim yo'nalishi -umumiy oʼrta taʼlimning boshlangʼich sinflarida “Ona tili”, “Oʼqish”, “Matematika”, “Mehnat”, “Tabiatshunoslik”,“Аtrofimizdagi olam”, oʼrta maxsus, kasb-xunar taʼlimi muassaslarida kasbiy fanlaridan oʼrnatilgan tartibda dars berish, umumiy oʼrta taʼlim maktablari, ixtisoslashtirilgan maktab, kasb-hunar kollejlari va maktabdan tashqari muassasalarda oʼqituvchi (tashkilotchi) boʼlib ishlash va boshqa faoliyat turlari bilan shug'ullanadigan o'quv dasturi",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '18,000,000 UZS',
  directionDescription:
    "Boshlang'ich ta'lim bakalavr ta'lim yo'nalishi -umumiy oʼrta taʼlimning boshlangʼich sinflarida “Ona tili”, “Oʼqish”, “Matematika”, “Mehnat”, “Tabiatshunoslik”,“Аtrofimizdagi olam”, oʼrta maxsus, kasb-xunar taʼlimi muassaslarida kasbiy fanlaridan oʼrnatilgan tartibda dars berish, umumiy oʼrta taʼlim maktablari, ixtisoslashtirilgan maktab, kasb-hunar kollejlari va maktabdan tashqari muassasalarda oʼqituvchi (tashkilotchi) boʼlib ishlash va boshqa faoliyat turlari bilan shug'ullanadigan o'quv dasturi",
  careerProspect:
    'Boshlangʼich taʼlim pedagogikasi va texnologiyasi, oʼqitishning didaktik vositalarini ishlab chiqish va amaliyotga joriy etish kabi faoliyat turlari bilan shugʼullanadi',
  workPlaces: [
    'Umumiy oʼrta taʼlim maktablari',
    'ixtisoslashtirilgan maktab',
    'kasb-hunar kollejlari va maktabdan tashqari muassasalarda oʼqituvchi (tashkilotchi)',
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const informationSystem = {
  directionName: 'Axborot tizimlari va texnologiyalari',
  directionInfo:
    "Axborot tizimlari va texnologiyalari bakalavriat taʻlim yoʻnalishi  - kompyuter va kommunikatsiya tarmoqlari texnikasining axborot tizimlari va ularda qo'llaniladigan texnologiyalar ishlash tartibi hamda qoidalarini o‘rnatish, dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida metodist, mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription: `Axborot tizimlari va texnologiyalari bakalavriat taʻlim yoʻnalishi  - kompyuter va kommunikatsiya tarmoqlari texnikasining axborot tizimlari va ularda qo'llaniladigan texnologiyalar ishlash tartibi hamda qoidalarini o‘rnatish, dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida metodist, mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.`,
  careerProspect:
    "Ta'lim bosqichida olingan IT sohasiga taaluqli bilim va ko'nikmalar  hisobiga quyidagi kasblar doirasida muvaffaqiyatli karyera qilish imkonini beradi: Axborot tizimlari va texnologiyalari muhandisi, AKT mutaxassisi; Frilanser dasturchi; Davlat xizmatchisi; O'qituvchi; Tizim va tarmoq administratori kabi faoliyat turlarida ishlashi mumkin.",
  workPlaces: [
    'IT muhandislik sohasida; Ilmiy-tadqiqot faoliyati',
    "Ta'lim sohasida; Dasturlash va frilanserlik sohasida",
    "Davlat tashkilotlari AKT bo'limlari",
    'Davlat xizmatlari sohasi',
    'Raqamli texnologiyalar vazirligi tizimida',
    "Raqamli texnologiyalar va sun'iy intellekt sohasida",
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const informationSecurity = {
  directionName: 'Kiberxavfsizlik injiniringi',
  directionInfo:
    "Kiberxavfsizlik injiniringi taʻlim yoʻnalishi - davlat va nodavkat korxona hamda tashkilotlari axborot havfsizligi siyosatini ishlab chiqish va nazorat qilish, ichki (korporativ) va tashqi (global) kompyuter tarmoqlari havfsizligini ta`minlash, shaxsiy va korporativ ma`lumotlar dahlsizligini ta`minlashga qaratilgan hamda ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Kiberxavfsizlik injiniringi taʻlim yoʻnalishi - davlat va nodavkat korxona hamda tashkilotlari axborot havfsizligi siyosatini ishlab chiqish va nazorat qilish, ichki (korporativ) va tashqi (global) kompyuter tarmoqlari havfsizligini ta`minlash, shaxsiy va korporativ ma`lumotlar dahlsizligini ta`minlashga qaratilgan hamda ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  careerProspect:
    "Ta'lim bosqichida olingan IT va Kiberxavfsizlik sohasiga taaluqli bilim va ko'nikmalar  hisobiga quyidagi kasblar doirasida muvaffaqiyatli karyera qilish imkonini beradi: Kiberxavfsizlik  muhandisi, Axborot xavfsizligini ta'minlash bo'yicha mutaxassis; Frilanser dasturchi; Davlat xizmatchisi; O'qituvchi; Tarmoq xavfsizligini ta'minlash administratori kabi faoliyat turlarida ishlashi mumkin.",
  workPlaces: [
    'IT muhandislik sohasida',
    'Ilmiy-tadqiqot faoliyati',
    "Ta'lim sohasida, Kiberxavfsizlikni ta'minlash markazida",
    "Davlat xavfsizlik xizmati mos yo'nalishlarida",
    'Dasturlash va frilanserlik sohasida',
    "Davlat tashkilotlari AKT/AX bo'limlari",
    'Davlat xizmatlari sohasi',
    'Raqamli texnologiyalar vazirligi tizimida',
    "Raqamli texnologiyalar va sun'iy intellekt sohasida",
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const computerEngineering = {
  directionName: 'Kompyuter injiniringi',
  directionInfo:
    "Kompyuter injiniringi taʻlim yoʻnalishida - kompyuter va kommunikatsiya tarmoqlari texnikalarining ishlash tartibi va qoidalarini o‘rganish, ularga xizmat ko'satish, dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida metodist, mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Kompyuter injiniringi taʻlim yoʻnalishida - kompyuter va kommunikatsiya tarmoqlari texnikalarining ishlash tartibi va qoidalarini o‘rganish, ularga xizmat ko'satish, dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida metodist, mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  careerProspect:
    "Ta'lim bosqichida olingan IT sohasiga taaluqli bilim va ko'nikmalar  hisobiga quyidagi kasblar doirasida muvaffaqiyatli karyera qilish imkonini beradi: Kompyuter muhandisi, AKT mutaxassisi; Frilanser dasturchi; Davlat xizmatchisi; O'qituvchi; Tizim va tarmoq administratori, Loyiha boshqaruvchisi (PM) kabi faoliyat turlarida ishlashi mumkin.",
  workPlaces: [
    'IT muhandislik sohasida',
    'Ilmiy-tadqiqot faoliyati',
    "Ta'lim sohasida",
    'Dasturlash va frilanserlik sohasida',
    "Davlat tashkilotlari AKT bo'limlari",
    'Davlat xizmatlari sohasi',
    'Raqamli texnologiyalar vazirligi tizimida',
    "Raqamli texnologiyalar va sun'iy intellekt sohasida",
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const softwareEngineering = {
  directionName: 'Dasturiy injiniring',
  directionInfo:
    "Dasturiy injiniring taʻlim yoʻnalishi - dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Dasturiy injiniring taʻlim yoʻnalishi - dasturiy ta’minot yaratish, algoritmlari va matematik ta’minotlarini ishlab chiqish, apparat-dasturiy tizimlarni loyihalash va ishlab chiqish va integratsiyalash, ilmiy-tadqiqot tashkilotlari,  umumiy o‘rta ta’lim maktablari, akademik litseylar, texnikumlarda “Kompyuter texnologiyalari va informatika” ga oid fanlardan o‘rnatilgan tartibda dars berish, mos ta’lim yo‘nalishlar bo‘yicha ta’limni amalga oshiruvchi vazirliklar, uning tarmoq boshqarmalari va muassasalarida mutaxassis hamda qonunchilik doirasida ruxsat etilgan barcha sohalarda ishlash uchun kadrlar tayyorlashga yo'naltirilgan o'quv dasturidir.",
  careerProspect:
    "Ta'lim bosqichida olingan IT sohasiga taaluqli bilim va ko'nikmalar  hisobiga quyidagi kasblar doirasida muvaffaqiyatli karyera qilish imkonini beradi: Dasturiy ta'minot muhandisi, AKT mutaxassisi; Frilanser dasturchi; Davlat xizmatchisi; O'qituvchi; Tizim va tarmoq administratori, Web dasturchi;  Loyiha boshqaruvchisi (PM)",
  workPlaces: [
    'IT muhandislik sohasida; Ilmiy-tadqiqot faoliyati',
    "Ta'lim sohasida, Dasturlash va frilanserlik sohasida",
    "Davlat tashkilotlari AKT bo'limlari",
    'Davlat xizmatlari sohasi',
    'Raqamli texnologiyalar vazirligi tizimida',
    "Raqamli texnologiyalar va sun'iy intellekt sohasida",
  ],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const economics = {
  directionName: 'Iqtisodiyot',
  directionInfo:
    "Iqtisodiyot bakalavriat ta’lim yo'nalishi - nazariy va amaliy iqtisod haqida to‘liq tushuncha beruvchi o'quv dasturidir.  Iqtisodni chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes va siyosat kabi fanlardan  foydalanadi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Iqtisodiyot bakalavriat ta’lim yo'nalishi - nazariy va amaliy iqtisod haqida to‘liq tushuncha beruvchi o'quv dasturidir.  Iqtisodni chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes va siyosat kabi fanlardan  foydalanadi.",
  careerProspect:
    "Iqtisodiyot bakalavriat ta`lim yo`nalishi - turli mulk shakllariga tegishli tarmoq, sohalarining xo`jalik yurituvchi subyetklari, moliya, kredit va sug`urta muassasalari, davlat hamda mahalliy hokimiyat organlari, ilmiy-tadqiqot tashkilotlari, umumiyta`lim, o`rta maxsus va oliy ta`lim muassasalarining iqtisodiy, moliya, marketing, ishlab chiqarish-iqtisodiy va tahlil xizmatlar faoliyatida, tarmoqlar va korxonalarini rivojlantirishga, istiqbolini belgilashga, kasbiy ko`nikmaga, mutasaddilik qobilyatiga yo`naltirilgan inson faoliyatining vositalari, usullari, metodlari va uslublari bilan bog`liq kompleks masalalarni hal etuvchi iqtisodchi bo'ladi.",
  workPlaces: ['Davlat tashkilotlari', 'xalqaro va mahalliy kompaniyalar', "ta'lim tashkilotlari"],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const marketing = {
  directionName: 'Marketing',
  directionInfo:
    "Marketing bakalavriat yo'nalishi - korxonada marketingni rolini ko'rsatish, marketingni boshqa funksiyalar bilan aloqasini o'rganish, xaridorlarning xatti-harakatini to'liq tushunishga qanchalik samarali marketing qurishni ko'rsatib beruvchi o'quv dasturidir.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Marketing bakalavriat yo'nalishi - korxonada marketingni rolini ko'rsatish, marketingni boshqa funksiyalar bilan aloqasini o'rganish, xaridorlarning xatti-harakatini to'liq tushunishga qanchalik samarali marketing qurishni ko'rsatib beruvchi o'quv dasturidir.",
  careerProspect:
    "Marketing bakalavriat ta 'lim yo 'nalishi - iqtisodiyot sohasidagi yo' nalish bo'lib, u davlat va hususiy sektor tizimida bozor talablarini o'rganuvchi, marketing bozorlari va is'temolchi hulq atvorini o'rganuvchi shug'ullanuvchi tashkilotlar, turli mulk shakllariga tegishli tarmoq, sohalarning xo'jalik yurituvchi subyektlari marketing tizimi, ta'lim muassasalari PR va marketing tarmog'i, xalqaro bozorning professional ishtirokchilari o'rtasida munosabat tizimi, oliy ta'lim muassasalarning marketing, mijozlar bilan munosabatlarni boshqarish tizimida faoliyat olib borishi mumkin.",
  workPlaces: ['Davlat tashkilotlari', 'xalqaro va mahalliy kompaniyalar', "ta'lim tashkilotlari"],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const management = {
  directionName: 'Menejment',
  directionInfo:
    "Menejment bakalavriat ta’lim yo'nalishida - nazariy va amaliy menejment haqida to‘liq tushuncha beriladi. Ushbu ixtisoslashgan darajada menejmentni chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes, siyosat korxonaning boshqaruv usullari, buxgalteriya, menejment asoslari va brend boshqaruvi kabi ko'plab boshqa fanlardan g'oyalar va usullardan foydalanadi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Menejment bakalavriat ta’lim yo'nalishida - nazariy va amaliy menejment haqida to‘liq tushuncha beriladi. Ushbu ixtisoslashgan darajada menejmentni chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes, siyosat korxonaning boshqaruv usullari, buxgalteriya, menejment asoslari va brend boshqaruvi kabi ko'plab boshqa fanlardagi g'oyalar va usullardan foydalanadi.",
  careerProspect:
    "Menejment bakalavriat ta`lim yo`nalishi - davlat, aksionerlik va xususiy kompaniyalar (firmalar), ishlab chiqarish korxonalari va birlashmalar, iqtisodiyotning turli soha va tarmoqlarini boshqarish, ularni ijtimoiy-iqtisodiy rivojlantirish istiqbollarini belgilash bo`yicha iqtisodiy, tashkiliy masalalar yechimlarini menejmentning rivojlanish qonunyatlari va zamonaviy konsepsiyalari asosida ishlab chiqarish va amalga oshirish bilan bog`liq kompleks masalalar hal etuvchi menejer bo'ladi.",
  workPlaces: ['Davlat tashkilotlari', 'xalqaro va mahalliy kompaniyalar', "ta'lim tashkilotlari"],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const finance = {
  directionName: 'Moliya va moliyaviy texnologiyalar',
  directionInfo:
    "Moliya va moliyaviy texnologiyalar bakalavriat ta’lim yo'nalishida - nazariy va amaliy moliya haqida to‘liq tushuncha beriladi. Ushbu ixtisoslashgan darajada moliyani chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes ,buxgalteriya, audit va moliyaviy bozorlar kabi fanlardagi g'oyalar va usullardan foydalanadi.",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Moliya va moliyaviy texnologiyalar bakalavriat ta’lim yo'nalishida - nazariy va amaliy moliya haqida to‘liq tushuncha beriladi. Ushbu ixtisoslashgan darajada moliyani chuqur o‘rganish davomida, talaba matematika, statistika, ekonometrika, biznes ,buxgalteriya, audit va moliyaviy bozorlar kabi fanlardagi g'oyalar va usullardan foydalanadi.",
  careerProspect:
    "Moliya bakalavriat ta 'lim yo 'nalishi - iqtisodiyot sohasidagi yo' nalish bo'lib, u davlat va korporativ moliya tizimi, moliyaviy bozorlar va moliyaviy vositachilik operatsiyalari bilan shug'ullanuvchi institutlar, turli mulk shakllariga tegishli tarmoq, sohalarning xo'jalik yurituvchi subyektlari moliyaviv xizmat tizimi, moliya, kredit va sug'urta muassasalari, moliyaviy bozorlar professional ishtirokchilari davlat hamda mahalliy hokimiyat organlari, ilmiy tadqiqot tashkilotlari, umumta'lim, o'rta maxsus va oliy ta'lim muassasalarning molivaviy, ishlab chiqarish-iqtisodiy va molivaviy tahlil xizmatlarini hal etuvchi moliyachi-iqtisodchi bo'ladi.",
  workPlaces: ['Davlat tashkilotlari', 'xalqaro va mahalliy kompaniyalar', "ta'lim tashkilotlari"],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const worldEconomy = {
  directionName:
    'Jahon iqtisodiyoti va xalqaro iqtisodiy munosabatlar (mintaqalar va faoliyat turlari bo‘yicha)',
  directionInfo:
    "Jahon iqtisodiyoti va xalqaro iqtisodiy munosabatlar bakalavriat ta'lim yo'nalishi - xalqaro iqtisodiyotning eng dolzarb muammolari, jumladan, tengsizlik, migratsiya, savdo urushlari  va iqlim o'zgarishlarini tanishtiruvchi o'quv dasturi",
  type: 'Kunduzgi',
  duration: '4 yil',
  credits: 240,
  semesters: 8,
  annualFee: '22,000,000 UZS',
  directionDescription:
    "Jahon iqtisodiyoti va xalqaro iqtisodiy munosabatlar bakalavriat ta'lim yo'nalishi - xalqaro iqtisodiyotning eng dolzarb muammolari, jumladan, tengsizlik, migratsiya, savdo urushlari  va iqlim o'zgarishlarini tanishtiruvchi o'quv dasturi hisoblanadi",
  careerProspect:
    "Jahon iqtisodiyoti va xalqaro iqtisodiy munosabatlar bakalavriat ta`lim yo`nalishi - turli mulk shakllariga tegishli xalqaro hamda mahalliy tarmoq, sohalarining xo`jalik yurituvchi subyetklari, moliya, kredit va sug`urta muassasalar, davlat hamda mahalliy hokimiyat organlari, ilmiy-tadqiqot tashkilotlari, umumiyta`lim, o`rta maxsus va oliy ta`lim muassasalarining iqtisodiy, xalqaro iqtisodiyot, iqtisodiy munosabatlar tizimi faoliyatida, xalqaro iqtisodiy tarmoqlar va korxonalarini rivojlantirishga, istiqbolini belgilashga, kasbiy ko`nikmaga, mutasaddilik qobilyatiga yo`naltirilgan inson faoliyatining vositalari, usullari, metodlari va uslublari bilan bog`liq kompleks masalalarni hal etuvchi xalqaro darajadagi iqtisodchi va menejer bo'ladi.    ",
  workPlaces: ['Davlat tashkilotlari', 'xalqaro va mahalliy kompaniyalar', "ta'lim tashkilotlari"],
  requirements: [
    {
      name: 'Diplom / transkript',
      description: 'faqat xalqaro talaba uchun',
    },
    {
      name: 'IELTS 5 or higher (TOEFL 35-45, CEFR B2)',
      description:
        "Agar malaka imtihonining asosiy tili ingliz tili bo'lsa, imtihondan ozod qilinishi mumkin",
    },
  ],
};

const getProgramData = (key: string | null) => {
  const access = key || 'economics';
  const data = {
    computerEngineering,
    economics,
    finance,
    marketing,
    management,
    preSchool,
    primaryEducation,
    informationSecurity,
    informationSystem,
    softwareEngineering,
    foreignLanguageEnglish,
    foreignLanguageKorean,
    worldEconomy,
    psychology,
  };

  return data[access as keyof typeof data];
};

export default getProgramData;
