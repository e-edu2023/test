import { post1 } from 'assets';
// import { post1, post2, post3, post4, post5 } from 'assets';

type Slide = {
  id: number;
  image: string;
  title: string;
};

const carouselList: Slide[] = [
  {
    id: 0,
    image: post1,
    title: 'admission',
  },
  // {
  //   id: 1,
  //   image: post1,
  //   title: 'videoAnimation',
  // },
  // {
  //   id: 1,
  //   image: post2,
  //   title: 'Application',
  // },
  // {
  //   id: 2,
  //   image: post3,
  //   title: 'Faculty insight',
  // },
  // {
  //   id: 3,
  //   image: post4,
  //   title: 'Seasonal event',
  // },
  // {
  //   id: 4,
  //   image: post5,
  //   title: 'Award',
  // },
];

export default carouselList;
