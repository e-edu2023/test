const menuList = [
  {
    key: 'university',
    link: '/university',
  },
  // {
  //   key: 'bachelorProgram',
  // },
  {
    key: 'forStudents',
  },
  // {
  //   key: 'campus',
  // },
  {
    key: 'admission',
    link: 'https://qabul.tiu.uz/uz',
  },
  {
    key: 'news',
  },
  {
    key: 'contactUs',
    link: '/contactUs',
  },
];

export default menuList;

// const menuList = [
//   'university',
//   'programming',
//   'forStudents',
//   'campus',
//   'admission',
//   'news',
//   'contactUs',
// ];
//
// export default menuList;
