export const defaultLanguage = "O'zbekcha";
export const fallbackLng = 'uz';
export const defaultMapCenter = [41.33346, 69.279505]; // [lat, lng]

export const defaultMapState = {
  center: defaultMapCenter,
  zoom: 16,
  controls: ['zoomControl', 'fullscreenControl'],
};

type Query = {
  lang?: 'tr_TR' | 'en_US' | 'en_RU' | 'ru_RU' | 'ru_UA' | 'uk_UA';
  apikey?: string;
  coordorder?: 'latlong' | 'longlat';
  load?: string;
  mode?: 'release' | 'debug';
  csp?: boolean;
  ns?: string;
};

export const YANDEX_QUERY: Query = {
  apikey: '8b1dac46-cb61-4fd5-9fcd-0a62c65fa204',
  lang: 'ru_RU',
};
