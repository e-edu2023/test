const contacts = [
  {
    key: 'hrDepartment',
    phone: '+998 (95) 131-55-55',
  },
  {
    key: 'marketingDepartment',
    phone: '+998 (95) 131-55-55',
  },
  {
    key: 'admissionDepartment',
    phone: '+998 (95) 131-55-55',
    link: 'admission@tiu.uz',
  },
  {
    key: 'rectorsOffice',
    phone: '+998 (95) 131-55-55',
    link: 'university@tiu.com',
  },
];

export default contacts;
