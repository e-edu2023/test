import uk from 'assets/programs/united-kingdom.png';
import korea from 'assets/programs/korea.png';
import BackpackIcon from 'assets/programs/Backpack';

import { ReactNode } from 'react';
import PieChart from 'assets/programs/PieChart';
import Dollar from 'assets/programs/Dollar';
import Trending from 'assets/programs/Trending';
import BriefCaseIcon from 'assets/programs/BriefCase';
import Monitor from 'assets/programs/Monitor';
import Code from 'assets/programs/Code';
import Psycho from 'assets/programs/Psycho';
import School from 'assets/programs/School';
import Database from 'assets/programs/Database';
import Shield from 'assets/programs/Shield';
import WorldIcon from 'assets/programs/World';

interface IProgram {
  title: string;
  direction: string;
  icon: ReactNode;
  type?: string;
  link: string;
}

const programs: IProgram[] = [
  {
    title: 'economics',
    direction: 'business',
    link: 'bachelorProgram',
    icon: <PieChart />,
  },
  {
    title: 'finance',
    direction: 'business',
    link: 'bachelorProgram',
    icon: <Dollar />,
  },
  {
    title: 'marketing',
    direction: 'business',
    link: 'bachelorProgram',
    icon: <Trending />,
  },
  {
    title: 'management',
    direction: 'business',
    link: 'bachelorProgram',
    icon: <BriefCaseIcon />,
  },
  {
    title: 'worldEconomy',
    direction: 'business',
    link: 'bachelorProgram',
    icon: <WorldIcon />,
  },
  {
    title: 'preSchool',
    direction: 'education',
    link: 'bachelorProgram',
    icon: <BackpackIcon />,
  },
  {
    title: 'primaryEducation',
    direction: 'education',
    link: 'bachelorProgram',
    icon: <School />,
  },
  {
    title: 'psychology',
    direction: 'education',
    link: 'bachelorProgram',
    icon: <Psycho />,
  },
  {
    title: 'informationSystem',
    direction: 'informationTechnologies',
    link: 'bachelorProgram',
    icon: <Database />,
  },
  {
    title: 'informationSecurity',
    direction: 'informationTechnologies',
    link: 'bachelorProgram',
    icon: <Shield />,
  },
  {
    title: 'computerEngineering',
    direction: 'informationTechnologies',
    link: 'bachelorProgram',
    icon: <Monitor />,
  },
  {
    title: 'softwareEngineering',
    direction: 'informationTechnologies',
    link: 'bachelorProgram',
    icon: <Code />,
  },
  {
    title: 'foreignLanguageEnglish',
    direction: 'foreignLanguage',
    link: 'bachelorProgram',
    icon: uk,
    type: 'image',
  },
  {
    title: 'foreignLanguageKorean',
    direction: 'foreignLanguage',
    link: 'bachelorProgram',
    icon: korea,
    type: 'image',
  },
];

export default programs;
