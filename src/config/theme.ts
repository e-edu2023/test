import { createTheme } from '@mui/material';

export default createTheme({
  typography: {
    fontFamily: 'Instrument Sans',
    button: {
      textTransform: 'none',
    },
  },
  components: {
    MuiTypography: {
      variants: [
        {
          props: {
            variant: 'subtitle2',
          },
          style: {
            color: '#425466',
            fontSize: 16,
            fontWeight: 400,
          },
        },
      ],
    },
  },

  palette: {
    info: {
      main: '#fff',
      contrastText: '#0A2540',
    },
  },
});
