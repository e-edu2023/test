import instagram from 'assets/socials/instagram-filled.svg';
import telegram from 'assets/socials/telegram.svg';
import youtube from 'assets/socials/Youtube.svg';
import facebook from 'assets/socials/Facebook.svg';
import instagramBlack from 'assets/contact/instagram-filled.svg';
import telegramBlack from 'assets/contact/telegram.svg';
import youtubeBlack from 'assets/contact/Youtube.svg';
import facebookBlack from 'assets/contact/Facebook.svg';

const socials = [
  {
    link: 'http://instagram.com/tiuuzb',
    icon: instagram,
    iconBlack: instagramBlack,
  },
  {
    link: 'https://t.me/tiuuzb',
    icon: telegram,
    iconBlack: telegramBlack,
  },
  {
    link: 'http://facebook.com/tiuuzb',
    icon: facebook,
    iconBlack: facebookBlack,
  },
  {
    link: 'https://www.youtube.com/@TIU_UZB',
    icon: youtube,
    iconBlack: youtubeBlack,
  },
];

export default socials;
