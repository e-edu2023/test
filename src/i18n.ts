import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import english from 'lang/en.json';
import uzbek from 'lang/uz.json';
import { fallbackLng } from 'config/config';

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng,
    lng: fallbackLng,
    supportedLngs: ['en', 'uz'],
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        translation: english,
      },
      uz: {
        translation: uzbek,
      },
    },
  });
