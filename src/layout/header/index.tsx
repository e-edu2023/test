import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Menu, Close } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import { Link } from 'react-router-dom';
import menuList from 'config/menuList';
import { logo } from 'assets';
// import DropDownMenu from './components/dropDownMenu';
import MenuDrawer from './components/menuDrawer';
import DropDownLanguage from './components/dropDownLanguage';
import './header.scss';

function Header() {
  const { t } = useTranslation();
  const [drawerOpen, setDrawerOpen] = useState<boolean>(false);

  const handleDrawerToggle = () => {
    setDrawerOpen(prevState => !prevState);
  };

  return (
    <div className='headerContainer'>
      <div className='nav'>
        <Link to='/'>
          <img className='logo' src={logo} alt='logo' />
        </Link>

        <div className='menuMobileBtn'>
          {drawerOpen ? (
            <IconButton size='large' color='inherit' onClick={handleDrawerToggle}>
              <Close />
            </IconButton>
          ) : (
            <IconButton size='large' color='inherit' onClick={handleDrawerToggle}>
              <Menu />
            </IconButton>
          )}
          <MenuDrawer open={drawerOpen} handleDrawerToggle={handleDrawerToggle} />
        </div>

        <div className='menuListContainer'>
          {menuList.map(({ key, link }) => (
            <div className='menuItem' key={key}>
              <div className='menuItemBox'>
                {link ? (
                  <Link
                    className='menuLink'
                    target={link.includes('http') ? '_blank' : '_self'}
                    to={link}
                  >
                    {t(`header.menu.${key}`)}
                  </Link>
                ) : (
                  <span className='menuLink'>{t(`header.menu.${key}`)}</span>
                )}
              </div>
              {/* <div className='dropDownMenuContainer'> */}
              {/*  <DropDownMenu menuType={item} /> */}
              {/* </div> */}
            </div>
          ))}
        </div>

        <div className='langContainer'>
          <DropDownLanguage />
        </div>
      </div>
    </div>
  );
}

export default Header;
