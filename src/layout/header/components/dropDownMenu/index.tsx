import React from 'react';
import { Link } from 'react-router-dom';
import styles from './dropDownMenu.module.scss';

interface DropDownMenuProps {
  menuType: string;
}

export const menu = {
  type: 'university',
  basePath: '/university',
  sections: [
    {
      id: '1',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
      ],
    },
    {
      id: '2',
      links: [
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
    {
      id: '3',
      links: [
        {
          name: 'Governance',
          path: '',
        },
        {
          name: 'Leaderships',
          path: '',
        },
        {
          name: 'Divisions',
          path: '',
        },
        {
          name: 'Faculties',
          path: '',
        },
        {
          name: 'Departments',
          path: '',
        },
      ],
    },
  ],
};

const DropDownMenu: React.FC<DropDownMenuProps> = () => (
    <div className={styles.container}>
      <div className={styles.content}>
        {menu.sections.map(section => (
          <div className={styles.section} key={section.id}>
            {section.links.map((link, i) => (
              <div className={styles.link} key={i}>
                <Link to={`${menu.basePath}${link.path}`}>{link.name}</Link>
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );

export default DropDownMenu;
