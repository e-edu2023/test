import React, { useState } from 'react';
import { MenuItem, Menu, Button } from '@mui/material';
import { defaultLanguage } from 'config';
import { useTranslation } from 'react-i18next';
import { arrowDownIcon, langIcon, searchIcon } from 'assets';
import './dropDownLanguage.scss';

const languages = [
  {
    name: 'English',
    key: 'en',
  },
  {
    name: "O'zbekcha",
    key: 'uz',
  },
];

function DropDownLanguage({ search = true }: { search?: boolean }) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [currentLanguage, setCurrentLanguage] = useState<string>(defaultLanguage);
  const {
    i18n: { changeLanguage },
  } = useTranslation();
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLanguageChange = (e: React.MouseEvent<HTMLLIElement>) => {
    setCurrentLanguage(e.currentTarget.getAttribute('data-language') || defaultLanguage);
    changeLanguage(e.currentTarget.getAttribute('data-key') || 'en');
    handleClose();
  };

  return (
    <div className='dropDownLanguage'>
      <Button color='inherit' onClick={handleClick}>
        <div className='languageIconBox'>
          <img src={langIcon} alt='language' />
        </div>
        <span> {currentLanguage}</span>
        <img src={arrowDownIcon} alt='arrowDown' />
      </Button>
      {search && (
        <div className='searchIconBox'>
          <img src={searchIcon} alt='search' />
        </div>
      )}

      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {languages.map(({ key, name }) => (
          <MenuItem
            data-language={name}
            data-key={key}
            selected={name === currentLanguage}
            key={key}
            onClick={handleLanguageChange}
          >
            {name}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}

export default DropDownLanguage;
