import React from 'react';
import { Accordion, AccordionSummary, Typography, AccordionDetails } from '@mui/material';
import { ExpandMore } from '@mui/icons-material';
import menuList from 'config/menuList';
import { useTranslation } from 'react-i18next';
import { menu } from '../../../dropDownMenu';

import styles from './menuAccordion.module.scss';

type MenuAccordionProps = {
  expandIcon?: boolean;
  sx?: object;
};

const navbar = menuList.map(({ key }) => ({
  key,
  menu,
}));

const MenuAccordion: React.FC<MenuAccordionProps> = ({ expandIcon = true, sx = {} }) => {
  const { t } = useTranslation();

  return (
    <div>
      {navbar.map(nav => (
        <Accordion
          sx={sx}
          key={nav.key}
          elevation={0}
          TransitionProps={{ unmountOnExit: true }}
          disableGutters
        >
          <AccordionSummary expandIcon={expandIcon ? <ExpandMore /> : null}>
            <Typography className={styles.title}>{t(`header.menu.${nav.key}`)}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            {nav.menu.sections.map(section =>
              section.links.map((link, i) => (
                <Typography key={i} className={styles.link} component='h5'>
                  {link.name}
                </Typography>
              )),
            )}
          </AccordionDetails>
        </Accordion>
      ))}
    </div>
  );
};

export default MenuAccordion;
