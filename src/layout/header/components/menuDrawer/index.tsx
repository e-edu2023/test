import React, { useState } from 'react';
import { Close, Search } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';
import { Divider, Drawer, IconButton, InputAdornment, OutlinedInput } from '@mui/material';
// import MenuAccordion from './component/menuAccordion';
import DropDownLanguage from 'layout/header/components/dropDownLanguage';
import menuList from 'config/menuList';
import { Link } from 'react-router-dom';
import styles from './menuDrawer.module.scss';

interface MenuDrawerProps {
  open: boolean;
  handleDrawerToggle: () => void;
}

const MenuDrawer: React.FC<MenuDrawerProps> = ({ open, handleDrawerToggle }) => {
  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const { t } = useTranslation();

  const onSearchFocus = () => {
    setIsSearchFocused(true);
  };
  const onExitButtonClick = () => {
    setIsSearchFocused(false);
  };

  return (
    <Drawer
      elevation={0}
      open={open}
      onClose={handleDrawerToggle}
      hideBackdrop
      sx={{
        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: '100%', top: '5rem' },
        top: '5rem',
      }}
    >
      <div className={styles.searchBox}>
        <OutlinedInput
          onFocus={onSearchFocus}
          startAdornment={
            <InputAdornment position='start'>
              <Search />
            </InputAdornment>
          }
          fullWidth
          label={null}
          placeholder={t('header.drawer.search.placeholder') || 'Search'}
          sx={{ borderRadius: '8px' }}
          inputProps={{
            sx: { padding: '0.5rem' },
          }}
        />
        {isSearchFocused && (
          <IconButton
            onClick={onExitButtonClick}
            aria-label='close'
            sx={{ borderRadius: '8px' }}
            color='inherit'
          >
            <Close />
          </IconButton>
        )}
      </div>
      <div className={styles.menuDrawerNavbar}>
        {menuList.map(({ link, key }) =>
          link ? (
            <Link
              key={key}
              onClick={handleDrawerToggle}
              className={styles.menuLink}
              target={link.includes('http') ? '_blank' : '_self'}
              to={link}
            >
              {t(`header.menu.${key}`)}
            </Link>
          ) : (
            <span key={key} className={styles.menuLink}>{t(`header.menu.${key}`)}</span>
          ),
        )}
      </div>
      {/* <MenuAccordion /> */}
      <div className={styles.languages}>
        <DropDownLanguage search={false} />
      </div>
      <Divider />
    </Drawer>
  );
};

export default MenuDrawer;
// <div className={styles.contacts}>
//   <Typography variant='body1'>HR department:</Typography>
//   <p>+998 (00) 000-00-00</p>
//   <Typography variant='body1'>Marketing department:</Typography>
//   <p>+998 (00) 000-00-00</p>
//   <Typography variant='body1'>Rector’s office</Typography>
//   <p>university@tiu.com</p>
// </div>
