import React from 'react';
import { Link } from 'react-router-dom';
import './footerTop.scss';
import MenuAccordion from '../../../header/components/menuDrawer/component/menuAccordion';

const sx = {
  backgroundColor: 'transparent',
  '&:before': {
    display: 'none',
  },
};

const FooterTop: React.FC = () => {
  return (
    <div className='footerTop'>
      <div className='footerTopContent'>
        {menu.sections.map((section) => (
          <div className='footerTopSection' key={section.id}>
            <div className='sectionTitle'>{section.title}</div>
            {section.links.map((link, i) => (
              <Link className='sectionLink' key={i} to={`${menu.basePath}${link.path}`}>
                {link.name}
              </Link>
            ))}
          </div>
        ))}
      </div>
      <div className='footerTopContentAccordion'>
        <MenuAccordion sx={sx} expandIcon={false} />
      </div>
    </div>
  );
};

export default FooterTop;

const menu = {
  type: 'university',
  basePath: '/university',
  sections: [
    {
      id: '1',
      title: 'University',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
    {
      id: '2',
      title: 'Programming',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
    {
      id: '3',
      title: 'For student',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
    {
      id: '4',
      title: 'Campus',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
    {
      id: '5',
      title: 'Admission',
      links: [
        {
          name: 'President‘s message',
          path: '',
        },
        {
          name: 'Virtual reception',
          path: '',
        },
        {
          name: 'About the university',
          path: '',
        },
        {
          name: 'Regulatory documents',
          path: '',
        },
        {
          name: 'University mission',
          path: '',
        },
      ],
    },
  ],
};
