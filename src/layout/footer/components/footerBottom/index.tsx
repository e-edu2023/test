import map from 'assets/circleMap.png';
import { useTranslation } from 'react-i18next';
import contacts from 'config/contact';
import socials from 'config/socials';
import './footerBottom.scss';
import { Link } from 'react-router-dom';

function FooterBottom() {
  const { t } = useTranslation();

  return (
    <div className='footerBottom'>
      <div className='footerBottomContent'>
        <div className='contentBottomContacts'>
          {contacts.map(contact => (
            <div key={contact.key}>
              <h5>{t(`footer.${contact.key}`)}</h5>
              {contact.link && <p>{contact.link}</p>}
              <p>{contact.phone}</p>
            </div>
          ))}
        </div>
        <div className='footerBottomMap'>
          <h5>{t('footer.address')}</h5>
          <div>
            <Link to='/contactUs'>
              <img className='footerMap' src={map} alt='map' />
            </Link>
          </div>
        </div>
        <div className='footerBottomMediaBlock'>
          <a className='linkBtn' href='https://qabul.tiu.uz/uz' target='_blank' rel='noreferrer'>
            {t('landingPage.carousel.content.apply')}
          </a>
          <div className='footerBottomSocials'>
            {socials.map(social => (
              <a key={social.link} rel='noreferrer' href={social.link} target='_blank'>
                <img src={social.icon} alt={social.link} />
              </a>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterBottom;
