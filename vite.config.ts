// eslint-disable-next-line import/no-extraneous-dependencies
import react from '@vitejs/plugin-react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { defineConfig } from 'vite';

export default defineConfig({
  plugins: [react()],

  resolve: {
    alias: {
      components: '/src/components',
      layout: '/src/layout',
      assets: '/src/assets',
      config: '/src/config',
      lang: '/src/lang',
      state: '/src/state',
      static: '/src/static',
      pages: '/src/pages',
    },
  },

  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "/src/variables.scss";`,
      },
    },
  },
});
